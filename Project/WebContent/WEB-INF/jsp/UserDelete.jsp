<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="model.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="Mock6.css">
<title>ユーザ削除確認</title>
</head>
<body>
	<form action="UserDeleteServlet" method="post">

		<div class="UserLost">

			<p>${userInfo.name}さん
				<a href="LogoutServlet">ログアウト</a>
			</p>
			<input type="hidden" name="id" value="${id }">

			<h1>ユーザ削除確認</h1>


			<p>ログインID:${loginId}</p>


			<p>を本当に削除してよろしいでしょうか。</p>

		</div>
		<a href="UserListServlet">戻る</a>
		<input type="submit" value="OK">
	</form>
</body>
</html>