<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="model.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>




<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="Mock5.css">
<title>ユーザ情報更新</title>
</head>
<body>


<form action="UserInfoNewUpServlet" method="post">

	<div class="UserInfoNew">

	<p>${userInfo.name}さん</p><a href = "LogoutServlet">ログアウト</a>

	<h1>ユーザ情報更新</h1>

	<span ><font color="red">${errMsg}</font></span><br>
<input type="hidden" name="id" value="${user.id}">

ログインID<input type="text" name="loginId" value="${user.loginId}" readonly="readonly" /><br>
パスワード<input type="password" name="passwordA"><br>
パスワード(確認)<input type="password" name="passwordB" id="passwordB"><br>
ユーザ名<input type="text" name="name" id="name" value="${user.name}"><br>
生年月日<input type="date" name="birthDate" max="9999-12-31" value="${user.birthDate}"><br>

<input type="hidden" name="updateDate" value="<%= System.currentTimeMillis() %>"><br>

<input type="submit" name="Bottun" value="更新"><br>

<div><a href="UserListServlet">戻る</a></div>

</div>
</form>
</body>
</html>