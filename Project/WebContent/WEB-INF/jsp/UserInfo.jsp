<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ page import="model.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="Mock4.css">
<title>ユーザ情報詳細参照</title>
</head>
<body>
<form action="UserInfoServlet" method="get">

<input type="hidden" name="id" value="${id }">


<p>${userInfo.name}さん<a href="LogoutServlet">ログアウト</a>


	<h1>ユーザ情報詳細参照</h1>



 <p>ログインID　　 ${user.loginId}</p>

 <p>ユーザ名　　　${user.name}</p>

 <p> 生年月日　　　${user.birthDate}</p>

 <p>登録日時　　　${user.createDate}</p>

 <p>更新日時　　　${user.updateDate}</p>




</form>
</body>
</html>