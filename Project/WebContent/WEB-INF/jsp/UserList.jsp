<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ page import="model.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>





<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="Mock2.css">
<title>ユーザ一覧</title>
</head>
<body>
	<form class="form-signin" action="UserListServlet" method="post">
		<div class="UserList">
			<p>${userInfo.name}さん</p>
			<a href="LogoutServlet">ログアウト</a>
			<h1>ユーザ一覧</h1>
			<p>
				<a href="UserNewServlet">新規登録</a>
			</p>


			ログインID <input type="text" name="loginId" id="loginId"><br>

			ユーザ名<input type="text" name="name" id="name"><br>

			生年月日<input
				type="date" name="birthDateA" max="9999-12-31">～<input
				type="date" name="birthDateB" max="9999-12-31"><br>


				<input type="submit" name="loginBottun"
					value="            検索          ">


			<table border="1">
				<thead>
					<tr><th>ログインID</th>
						<th>ユーザ名</th>
						<th>生年月日</th>
						<th></th>
					</tr>
				</thead>
				<tbody>

					<c:forEach var="user" items="${userList}">
						<tr>

						<td>${user.loginId}</td>
						<td>${user.name}</td>
						<td>${user.birthDate}</td>

							<td>
							<a href="UserInfoServlet?id=${user.id}">詳細</a>
							<c:if test="${userInfo.loginId == 'admin' or userInfo.loginId == user.loginId}">
							<a href="UserInfoNewUpServlet?id=${user.id}">更新</a></c:if>
							<c:if test="${userInfo.loginId == 'admin'}">
							<a href="UserDeleteServlet?id=${user.id}">削除</a></c:if>
							</td>
							</tr>

</c:forEach>
			</table>
		</div>
	</form>
</body>
</html>