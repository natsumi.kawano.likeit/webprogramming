
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserNewServlet
 */
@WebServlet("/UserNewServlet")
public class UserNewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserNewServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
		}


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserNew.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String passwordA = request.getParameter("passwordA");
		String passwordB = request.getParameter("passwordB");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		UserDao userDao = new UserDao();

		User user = userDao.newUse(loginId);

		try {
			userDao.Md5(passwordA);
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		//パスワードが同じ値
		//全てのフォームに入力されてるのとき登録可能

		boolean isError = false;

		if (user != null) {
			isError = true;
		}

		if (!(passwordA.equals(passwordB))) {
			isError = true;
		}

		//フォームの未入力にはエラーを出す

		if (loginId.length() == 0 || passwordA.length() == 0 || passwordB.length() == 0 || name.length() == 0
				|| birthDate.length() == 0) {
			isError = true;
		}

		if(isError) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			User user2 = new User();
			user2.setLoginId(loginId);
			user2.setPasswordA(passwordA);
			user2.setPasswordB(passwordB);
			user2.setName(name);
			user2.setBirthDate(birthDate);


			request.setAttribute("user", user2);


			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserNew.jsp");
			dispatcher.forward(request, response);
			return;
		}

		userDao.userInsert(loginId, passwordA, name, birthDate);
		response.sendRedirect("UserListServlet");

	}
}
