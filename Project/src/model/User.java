package model;
import java.io.Serializable;

public class User implements Serializable {

	private int id;
	private String loginId;
	private String name;
	private String birthDate;
	private String passwordA;
	private String passwordB;
	private String createDate;
	private String updateDate;

	public User() {

	}

	public User(String loginId, String name) {
		this.loginId = loginId;
		this.name = name;
	}

	// 全てのデータをセットするコンストラクタ
	public User(int id, String loginId, String name, String birthDate, String passwordA,String passwordB, String createDate,
			String updateDate) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.passwordA = passwordA;
		this.passwordB = passwordB;
		this.createDate = createDate;
		this.updateDate = updateDate;


	}


	public User(int id,String loginId, String name, String birthDate, String createDate,String updateDate) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}


	public User(String loginId, String name, String birthDate, String createDate,String updateDate) {

		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	public User(int id,String passwordA, String passwordB, String name, String birthDate) {

		this.id = id;
		this.passwordA = passwordA;
		this.passwordB = passwordB;
		this.name = name;
		this.birthDate = birthDate;

	}





	public User(String loginIdDate) {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public User(String loginId, String password, String name, String birthDate, String createDate,
			String updateDate) {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getPasswordA() {
		return passwordA;
	}

	public void setPasswordA(String passwordA) {
		this.passwordA = passwordA;
	}



	public String getPasswordB() {
		return passwordB;
	}

	public void setPasswordB(String passwordB) {
		this.passwordB = passwordB;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

}
