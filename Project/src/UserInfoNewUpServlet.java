

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;


/**
 * Servlet implementation class UserInfoNewUpServlet
 */
@WebServlet("/UserInfoNewUpServlet")
public class UserInfoNewUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserInfoNewUpServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
		}



		//主キーにあたるデータを取得
		String id = request.getParameter("id");

		UserDao userDao = new UserDao();
		User user = userDao.empUp(id);

		//表示するログインIDとnameを取得
		request.setAttribute("id", id);
		request.setAttribute("loginId", user.getLoginId());
		request.setAttribute("user", user);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserInfoNewUp.jsp");
		dispatcher.forward(request,response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");
		String loginId = request.getParameter("loginId");
		String passwordA = request.getParameter("passwordA");
		String passwordB = request.getParameter("passwordB");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");


		 UserDao dao = new UserDao();

		 try {
				dao.Md5(passwordA);
			} catch (NoSuchAlgorithmException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

		 boolean isErro = false;

		 if(!(passwordA.equals(passwordB))){

			 isErro = true;
		 }

		 if(name.length() == 0 || birthDate.length() == 0){

			 isErro = true;
		 }

		 if(isErro) {
			 request.setAttribute("errMsg", "入力された内容は正しくありません");


				User user2 = new User();
				user2.setId(Integer.parseInt(id));
				user2.setLoginId(loginId);
				user2.setPasswordA(passwordA);
				user2.setPasswordB(passwordB);
				user2.setName(name);
				user2.setBirthDate(birthDate);
				request.setAttribute("user", user2);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserInfoNewUp.jsp");
				dispatcher.forward(request, response);
			return;
		 }

		 dao.UserUpDate(Integer.parseInt(id),passwordA,name,birthDate);
		 dao.UserUp(Integer.parseInt(id),name,birthDate);
		 response.sendRedirect("UserListServlet");


	}

}



