package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;


//passwordは確認用が存在しても、中身は同じなので重複させる必要はない。


/**
 * ユーザテーブル用のDao
 * @author takano
 *
 */
public class UserDao {

	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す
	 * @param loginId
	 * @param password
	 * @return
	 */
	public User findByLoginInfo(String loginId, String passwordA) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, passwordA);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * 全てのユーザ情報を取得する
	 * @return
	 */
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user where login_id <> 'admin'";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String birthDate = rs.getString("birth_date");
				String passwordA = rs.getString("password");
				String passwordB = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, passwordA, passwordB, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}


	public List<User> findSearch(String loginIdP,String nsme,String birthDateP,String birthDateS) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user where login_id <> 'admin'";

			if(loginIdP.length() > 0) {
				sql += " and login_id = '" + loginIdP + "'";
			}

			if(nsme.length() > 0) {
				sql += " and name LIKE '%" + nsme + "%'";
			}

			if(!birthDateP.equals("")) {
				sql += " and birth_date >= '" + birthDateP + "'";
			}

			if(!birthDateS.equals("")) {
				sql += " and birth_date <= '" + birthDateS + "'";
			}


			System.out.println(sql);
			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);


			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String birthDate = rs.getString("birth_date");
				String passwordA = rs.getString("password");
				String passwordB = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, passwordA, passwordB, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public User newUse(String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);

			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdDate = rs.getString("login_id");

			return new User(loginIdDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public User empUp(String Id) {

		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "SELECT login_id, name, birth_date, create_date, update_date FROM user WHERE id =" + Id;

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			if (!rs.next()) {

				return null;
			}

			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			String birthDate = rs.getString("birth_date");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");

			return new User(Integer.parseInt(Id),loginId, name, birthDate, createDate, updateDate);

		} catch (SQLException e) {

			e.printStackTrace();

			return null;

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

	public User UserInfo(String loginId, String name, String birthDate,String updateDate) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT login_id, name, birth_date, update_date FROM user WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, name);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, updateDate);

			ResultSet rs = pStmt.executeQuery();

			// ログイン失敗時の処理		・・・①
			if (!rs.next()) {
				return null;
			}

			// ログイン成功時の処理		・・・②
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();

			return null;

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}
	}

	public void userInsert(String loginId, String password, String name, String birthDate) {
		Connection conn = null;

		try {
			// データベース接続
			conn = DBManager.getConnection();
			// 実行SQL文字列定義
			String sql = "INSERT INTO user(login_id, password, name, birth_date, create_date, update_date) VALUES(?,?,?,?,now(),now())";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			// SQLの?パラメータに値を設定
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			pStmt.setString(3, name);
			pStmt.setString(4, birthDate);

			// 登録SQL実行
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void UserDelete(int id) {

		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "DELETE FROM user WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setInt(1, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			if (conn != null) {

				try {

					conn.close();

				} catch (SQLException e) {

					e.printStackTrace();

				}

			}

		}

	}

	public void UserUpDate(int id,String password, String name, String birthDate) {

		//No value specified for parameter 4　とはsql文の？？？？が４つなのに対して
		//pStmt.setStringの数が４つないときに出る。数をそろえてあげる。

		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "UPDATE user SET password = ?, name = ?, birth_date = ?, update_date = now() WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1,password);
			pStmt.setString(2, name);
			pStmt.setString(3,birthDate);
			pStmt.setInt(4,id);


			pStmt.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			if (conn != null) {

				try {

					conn.close();

				} catch (SQLException e) {

					e.printStackTrace();

				}

			}

		}

	}

	public void UserUp(int id, String name, String birthDate) {

		//No value specified for parameter 4　とはsql文の？？？？が４つなのに対して
		//pStmt.setStringの数が４つないときに出る。数をそろえてあげる。

		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "UPDATE user SET name = ?, birth_date = ?, update_date = now() WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, name);
			pStmt.setString(2,birthDate);
			pStmt.setInt(3,id);


			pStmt.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			if (conn != null) {

				try {

					conn.close();

				} catch (SQLException e) {

					e.printStackTrace();

				}

			}

		}

	}

	public String Md5(String password) throws NoSuchAlgorithmException{

		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		String result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		System.out.println(result);

		return result;
}






	}




